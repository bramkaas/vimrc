syntax enable
set tabstop=4
set shiftwidth=4
set expandtab
set number
filetype indent on
set autoindent

:nnoremap ; l
:nnoremap l k
:nnoremap k j
:nnoremap j h

:nnoremap <buffer> <F5> :exec '!python3' shellescape(@%, 1)<cr>
:nnoremap <buffer> <F6> :!gcc % -o %<  && ./%< <cr>
